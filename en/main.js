$(window).on("load", function() {
  $('.loading-screen').delay(1500).fadeOut('slow');
  setTimeout(function(){
       $('body').addClass('loaded');
   }, 1400);
})
  $(document).ready(function() {
    $("#mobile").on("blur", function(e){
       var myval = $(this).val();

       if(myval.length < 10) {
          $(this).addClass('input-error');
            $(this).focus();
       }
       else {
         $(this).removeClass('input-error');
       }
  });
    var $animation_elements = $('.site-section');
    var $window = $(window);

    function check_if_in_view2() {
      var window_height = $window.height();
      var window_top_position = $window.scrollTop();
      var window_bottom_position = (window_top_position + window_height);

      $.each($animation_elements, function() {
        var $element = $(this);
        var element_height = $element.outerHeight();
        var element_top_position = $element.offset().top;
        var element_bottom_position = (element_top_position + element_height);

        //check to see if this current container is within viewport
        if ((element_bottom_position >= window_top_position) &&
          (element_top_position <= window_bottom_position)) {
          $element.addClass('in-view');
          $(".site-nav__primary li").removeClass("active");
          if ($(this).prop('id') == "spotlight") {
            $("a[href='#spotlight']").parent().addClass("active");
          }
          if ($(this).prop('id') == "features") {
            $("a[href='#features']").parent().addClass("active");
          }
          if ($(this).prop('id') == "contact") {
            $("a[href='#contact']").parent().addClass("active");
          }
          if ($(this).prop('id') == "request") {
            $("a[href='#request']").parent().addClass("active");
          }
        } else {
          $element.removeClass('in-view');
        }
      });
    }

    $window.on('scroll resize', check_if_in_view2);
    $window.trigger('scroll');
    $(".site-nav__primary ul li").off().on("click", function() {
      $(".site-nav__primary ul li").removeClass("active");
      $(this).addClass("active");
      $(".site-nav,.nav-toggle").toggleClass("active");

    });
    $(".nav-toggle").off().on("click", function() {
      $(this).toggleClass("active");
      $(".site-nav").toggleClass("active");
    });
    //for smooth scroll

    function checkPosition() {
        if (window.matchMedia('(max-width: 767px)').matches) {
          $('.site-nav__primary ul li a').click(function(){
              $('html, body').animate({
                  scrollTop: $( $(this).attr('href') ).offset().top-40
              }, 500);
              $(".nav-toggle").toggleClass("active");
              $(".site-nav").toggleClass("active");
              return false;
          });
        } else {
          $('.site-nav__primary ul li a').click(function(){
              $('html, body').animate({
                  scrollTop: $( $(this).attr('href') ).offset().top-60
              }, 500);
              $(".nav-toggle").toggleClass("active");
              $(".site-nav").toggleClass("active");
              return false;
          });
        }
    }
      checkPosition();
  });
