$(document).ready(function() {

    // $("#inquire_now").on('click', function(e) {
    //     $("#inquire_popup").addClass("show");
    //
    // });

    // $("#close").on('click', function(e) {
    //     $("#inquire_popup").removeClass("show");
    // });

    //var iti = $("#phoneInq").intlTelInput({initialCountry: "ae", utilsScript: "./scripts/utils.js"});

    var x = document.getElementById('div_for_inq_pop');

    x.innerHTML = popup;

    var iti = $("#phoneInq").intlTelInput({initialCountry: "ae", utilsScript: "./scripts/utils.js"});

    $("#close").on('click', function(e) {
        $("#inquire_popup").removeClass("show");
    });

});

// <!--igor changes here-->
let popup = '<div class="footer__newsletter_subs2" id="inquire_popup">'+
    '<div class="footer__newsletter_subs__content">'+
    '<div class="container no_pad" style="color: #0b0b0b;">'+
    '<div class="row">'+
    '<div class="col-xs-24">'+
    '<div class="contact-form col-md-16 col-md-offset-4 col-xs-24 contact-form" id="contactform">'+
    '<span id="close" id="inquire_now_hide">x</span>Register your interest'+
    '<form action="send-email.php" class="elq-form" id="inquireFrom" method="post" name="ContactFormNakheelInq">'+
    '<div class="contact-form__inputs">'+
    '<input id="firstNameInq" name="firstName" placeholder="First Name" style="color: #0b0b0b" type="text">'+
    '<input id="lastNameInq" name="lastName" placeholder="Last Name" style="color: #0b0b0b" type="text">'+
    '</div>'+
    '<div class="contact-form__inputs">'+
    '<input id="emailAddressInq" name="emailAddress" placeholder="Email Address" style="color: #0b0b0b" type="email">'+
    '</div>'+
    '<div class="contact-form__inputs">'+
    '<input id="phoneCode2" name="phoneCountryCode1" type="hidden" />'+
    '<input id="phoneInq" name="busPhone" style="margin-left: 0px !important; color: #0b0b0b" type="tel">'+
    '</div>'+
    '<div class="contact-form__textarea">'+
    '<textarea id="Message" name="Message" placeholder="Tell Us How We Can Help You">'+'</textarea>'+
    '</div>'+
    '<div class="contact-form__inputs">'+
    '<button class="btn btn-outline_blue col-xs-12 col-xs-offset-6" id="submit_form_inquire" onclick="didSelectInquireFormSubmit(event)">Submit</button>'+
    '</div>'+
    '<input name="elqSiteId" type="hidden" value="839945550" />'+
    '<input name="elqCampaignId" type="hidden" />'+
    '<input name="elqCustomerGUID" type="hidden" value="">'+
    '<input name="elqCookieWrite" type="hidden" value="0">'+
    '<input id="type" name="Type" type="hidden" >'+
    '<input id="property" name="Property" type="hidden">'+
    '<input name="elqFormName" type="hidden" value="ContactFormNakheel2019">'+
    '</form>'+
    '</div>'+
    '</div>'+
    '</div>'+
    '</div>'+
    '</div>'+
    '</div>'+
    '<div class="footer__newsletter_subs" id="success_form_inq">'+
    '<div class="footer__newsletter_subs__content">'+
    '<div class="container no_pad">'+
    '<div class="row">'+
    '<div class="col-xs-24">'+
    '<h2>Thank you !</h2>'+
    '<h3>We will get back to you soon.</h3>'+
    '<div><a class="btn btn-outline_white">Back</a></div>'+
    '</div>'+
    '</div>'+
    '</div>'+
    '</div>'+
    '</div>';


function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}


function validateInqForm() {

    let v = true;

    if($("#firstNameInq").val() == ''){
        $("#firstNameInq").addClass('error')
        v = false;
    }else{
        $("#firstNameInq").removeClass('error')
    }

    if($("#lastNameInq").val() == ''){
        $("#lastNameInq").addClass('error')
        v = false;
    }else{
        $("#lastNameInq").removeClass('error')
    }

    if($("#emailAddressInq").val() == ''){
        $("#emailAddressInq").addClass('error')
        v = false;
    }else{
        if(validateEmail($("#emailAddressInq").val())){
            $("#emailAddressInq").removeClass('error')
        }else{
            $("#emailAddressInq").addClass('error')
            v = false;
        }

    }

    if($("#phoneInq").val() == ''){

        $("#phoneInq").addClass('error')
        v = false;

    }else{
        if ($.trim($("#phoneInq").intlTelInput("getNumber"))) {

            if ($("#phoneInq").intlTelInput("isValidNumber")) {
                //done
                $("#phoneInq").removeClass('error')

                let code = $("#phoneInq").intlTelInput("getSelectedCountryData").dialCode;

                $("#phoneCode2").val(code);


            } else {
                //error phone
                $("#phoneInq").addClass('error')
                v = false;

            }
        }
    }

    if($("#Message").val() == ''){
        $("#Message").addClass('error')
        v = false;
    }else{
        $("#Message").removeClass('error')
    }


    return v;

}

function didSelectInquireFormSubmit(e) {
    e.preventDefault();

    if (!validateInqForm()) return;
    // <!--igor changes here-->
    $.post("send-email.php",
        $("#inquireFrom").serialize(),
        function(data) {
            $("#inquire_popup").removeClass("show");
            $("#success_form_inq").addClass("show");
        }
    );
}
